## Clone SD-CARD with raspbian in it
* Unmount card
* `sudo bash clone.sh /dev/mmcblk0 rpi.img.zip`
* Sample output
  ```e2fsck 1.43.5 (04-Aug-2017)
        Pass 1: Checking inodes, blocks, and sizes
        Pass 2: Checking directory structure
        Pass 3: Checking directory connectivity
        Pass 4: Checking reference counts
        Pass 5: Checking group summary information
        rootfs: 206660/1867136 files (0.1% non-contiguous), 1476674/7582208 blocks
        Warning: Shrinking a partition can cause data loss, are you sure you want to continue?
        Information: You may need to update /etc/fstab.

        6900+0 records in
        6900+0 records out
        7235174400 bytes (7.2 GB, 6.7 GiB) copied, 115.547 s, 62.6 MB/s
        adding: rpi.img (deflated 68%)
        Information: You may need to update /etc/fstab.

        resize2fs 1.43.5 (04-Aug-2017)
        Resizing the filesystem on /dev/mmcblk0p2 to 7580997 (4k) blocks.
        The filesystem on /dev/mmcblk0p2 is now 7580997 (4k) blocks long.
      ```

## Writing the cloned image on new card
* Format the sd-card
* `unzip -p rpi.img.zip | sudo dd bs=1M of=/dev/mmcblk0`
* Sample Output
  ```0+81648 records in
        0+81648 records out
        7235174400 bytes (7.2 GB, 6.7 GiB) copied, 457.817 s, 15.8 MB/s
  ```

### Script credits
* Changes are made according to our requirement
* https://github.com/zymbit/rpi-mkimg