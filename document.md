## piGPIO extension
* No problem when deleted
* Always stays green when loaded
* No error or alert on loaded
* No error or alert on execution of blocks

## piSenseHATE extension
* No problem when deleted
* Stays red till sensehate is not connected
* Alert that no hardware is connected when loaded
* No error or alert on execution of blocks

## Dynamic update of blocks
* Only lines to update blocks(Change `descriptorJSON` to check changes in block)
```
ScratchExtensions.unregister('EXTENSION NAME');
ScratchExtensions.register('EXTENSION NAME', descriptorJSON, extensionID);
```
* Block ID is the function name given in second column of individual blocks array(stating from 0)
* If the blockID is removed the block on the programming stage becomes undefined
* After re registrating block updates(on the programming stage) only if the blockID remains same

## Desktop environment for RPI3
* With raspbian lite GUI applications don't work as they need display manager(GUI applications need a display manager to start)
* Minibian (https://sourceforge.net/projects/minibian/files/)
* Tiny Core (http://tinycorelinux.net/welcome.html)
* Restricting the number of applications at a single point of time(https://askubuntu.com/questions/483554/how-do-i-get-a-list-of-running-applications-by-using-the-command-line)
* Install full raspbian complete raspbian and remove unwanted software and install all custom software; Custom package the current build
* Building our own custom bulid for RPI3 (https://github.com/andrius/build-raspbian-image)
* Run GUI applications without desktop (https://raspberrypi.stackexchange.com/questions/12606/run-a-gui-without-the-desktop) (https://raspberrypi.stackexchange.com/questions/34330/raspbian-without-a-gui-and-other-programs-i-dont-need)

## Startup | Shutdown | Reboot (execute only scripts)
* https://raspberrypi.stackexchange.com/questions/15475/run-bash-script-on-startup
* https://raspberrypi.stackexchange.com/questions/15571/how-to-add-start-up-and-shutdown-script (Runlevels)

## Hotspot
* For hotspot related check hotspot.md and hotspot folder(for scripts)
* https://medium.com/@edoardo849/turn-a-raspberrypi-3-into-a-wifi-router-hotspot-41b03500080e
* https://learn.adafruit.com/setting-up-a-raspberry-pi-as-a-wifi-access-point?view=all

## Write protection for files
* `chattr +i file.ext` - With root as owner as file
* `+i` for immutable flag
* `-i` for mutable flag
* `+a` for appendable flag(Content cannot be deleted but appended)
* For more `man chattr`