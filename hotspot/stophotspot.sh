#!/bin/bash
echo "Killing nohup hotspot process"
kill -9 `cat /usr/share/nexted/hotspot/hotspot_pid.txt`
rm /usr/share/nexted/hotspot/hotspot_pid.txt
echo "Enabling wireless interface"
rm /etc/wpa_supplicant/wpa_supplicant.conf
mv /etc/wpa_supplicant/wpa_supplicant.conf.orig /etc/wpa_supplicant/wpa_supplicant.conf
echo "Adding wlan0 interface"
rm /etc/dhcpcd.conf
cp /usr/share/nexted/hotspot/original/dhcpcd.conf /etc/dhcpcd.conf
echo "Setting up DHCP"
rm /etc/dhcp/dhcpd.conf
cp /usr/share/nexted/hotspot/original/dhcpd.conf /etc/dhcp/dhcpd.conf
echo "Setting up ISC DHCP Server"
rm /etc/default/isc-dhcp-server
cp /usr/share/nexted/hotspot/original/isc-dhcp-server /etc/default/isc-dhcp-server
echo "Shutting down wlan0"
ifconfig wlan0 down
echo "Setting up network interface"
rm /etc/network/interfaces
cp /usr/share/nexted/hotspot/original/interfaces /etc/network/interfaces
echo "Unblocking sofblock"
rfkill unblock wifi
rfkill unblock all
echo "Setting up static IP for wlan0"
ifconfig wlan0 dynamic
echo "Setting up hostapd"
rm /etc/hostapd/hostapd.conf
echo "Setting up network translation"
rm /etc/sysctl.conf
cp /usr/share/nexted/hotspot/original/sysctl.conf /etc/sysctl.conf
echo "Restarting network manager"
ifconfig wlan0 down
systemctl daemon-reload
systemctl restart dhcpcd
ifconfig wlan0 up
service networking restart