#!/bin/bash
########################Example calls######################
# sudo ./starthotspot.sh -r abababab # With hotspot ssid from inline
# sudo ./starthotspot.sh -c # With hotspot ssid from config
###########################################################
#sudo apt-get update
#sudo apt-get install hostapd isc-dhcp-server
if [ $# -eq 0 ]
then
    echo "Please give arguments as specified"
else
    echo "Killing nohup hotspot process"
    kill -9 `cat /usr/share/nexted/hotspot/hotspot_pid.txt`
    rm /usr/share/nexted/hotspot/hotspot_pid.txt
    echo "Removing wireless interface"
    mv /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf.orig
    cp /usr/share/nexted/hotspot/edited/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
    ifconfig wlan0 down
    systemctl daemon-reload
    systemctl restart dhcpcd
    ifconfig wlan0 up
    source /usr/share/nexted/hotspot/config
    if [[ $1 == "-c" ]]
    then
        echo "Updating hostapd"
        sed -i "1s/.*/$PRESSID$SSID/" /usr/share/nexted/hotspot/edited/hostapd.conf
        sed -i "2s/.*/$PREPASS$PASSWORD/" /usr/share/nexted/hotspot/edited/hostapd.conf
    else
        echo "Updating hostapd"
        sed -i "1s/.*/$PRESSID$2/" /usr/share/nexted/hotspot/edited/hostapd.conf
        sed -i "2s/.*/$PREPASS$2/" /usr/share/nexted/hotspot/edited/hostapd.conf
    fi
    echo "Denying wlan0 interface"
    rm /etc/dhcpcd.conf
    cp /usr/share/nexted/hotspot/edited/dhcpcd.conf /etc/dhcpcd.conf
    echo "Setting up DHCP"
    rm /etc/dhcp/dhcpd.conf
    cp /usr/share/nexted/hotspot/edited/dhcpd.conf /etc/dhcp/dhcpd.conf
    echo "Setting up ISC DHCP Server"
    rm /etc/default/isc-dhcp-server
    cp /usr/share/nexted/hotspot/edited/isc-dhcp-server /etc/default/isc-dhcp-server
    echo "Shutting down wlan0"
    ifconfig wlan0 down
    echo "Setting up network interface"
    rm /etc/network/interfaces
    cp /usr/share/nexted/hotspot/edited/interfaces /etc/network/interfaces
    echo "Unblocking sofblock"
    rfkill unblock wifi
    rfkill unblock all
    echo "Setting up static IP for wlan0"
    ifconfig wlan0 192.168.42.1
    echo "Setting up hostapd"
    cp /usr/share/nexted/hotspot/edited/hostapd.conf /etc/hostapd/hostapd.conf
    echo "Setting up network translation"
    rm /etc/sysctl.conf
    cp /usr/share/nexted/hotspot/edited/sysctl.conf /etc/sysctl.conf
    echo "Activating network translation"
    sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
    echo "Modifying IPtables"
    iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
    iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
    iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
    echo "Making routing happen"
    sh -c "iptables-save > /etc/iptables.ipv4.nat"
    echo "Restarting services"
    service hostapd restart
    service isc-dhcp-server restart
    echo "Starting the hostspot"
    nohup  /usr/sbin/hostapd /etc/hostapd/hostapd.conf &
    echo $! > /usr/share/nexted/hotspot/hotspot_pid.txt
fi