### Check pipcdocuments/hotspot folder for all the scripts

## Installation
```
sudo apt-get update
sudo apt-get install hostapd isc-dhcp-server
````

## Denying wlan0 interface
* Backup original file: `/etc/dhcpcd.conf` to `/lib/hotspot/original/dhcpcd.conf`
* Making changes in: `/etc/dhcpcd.conf`
* Backup edited file: `/etc/dhcpcd.conf` to `/lib/hotspot/edited/dhcpcd.conf`

## Changing DHCP server
* Backup original file: `/etc/dhcp/dhcpd.conf` to `/lib/hotspot/original/dhcpd.conf`
* Making changes in `/etc/dhcp/dhcpd.conf`
* Backup edited file: `/etc/dhcp/dhcpd.conf` to `/lib/hotspot/edited/dhcpd.conf`

## Changing isc-dhcp-server
* Backup original file: `/etc/default/isc-dhcp-server` to `/lib/hotspot/original/isc-dhcp-server`
* Making changes in `/etc/default/isc-dhcp-server`
* Backup edited file: `/etc/default/isc-dhcp-server` to `/lib/hotspot/edited/isc-dhcp-server`

## Shut down wlan0
* `sudo ifconfig wlan0 down`

## Changing Network interface
* Backup original file: `/etc/network/interfaces` to `/lib/hotspot/original/interfaces`
* Making changes in `/etc/network/interfaces`
* Backup edited file: `/etc/network/interfaces` to `/lib/hotspot/edited/interfaces`

## Making static IP for RPI
* `sudo ifconfig wlan0 192.168.42.1`
*n Error Occured --> `SIOCSIFFLAGS: Operation not possible due to RF-kill`*

## Check Soft-blocking
https://askubuntu.com/questions/62166/siocsifflags-operation-not-possible-due-to-rf-kill
```
sudo rfkill unblock wifi
sudo rfkill unblock all	
```
* `sudo ifconfig wlan0 192.168.42.1`

## Setting up Hostapd
* Creating `sudo nano /etc/hostapd/hostapd.conf`
* Backup edited file: `sudo nano /etc/hostapd/hostapd.conf` to `/lib/hotspot/edited/hostapd.conf`

## Setting up network translation
* Backup original file: `/etc/sysctl.conf` to `/lib/hotspot/original/sysctl.conf`
* Making changes in `/etc/sysctl.conf`
* Backup edited file: `/etc/sysctl.conf` to `/lib/hotspot/edited/sysctl.conf`

## Activating the above
* `sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"`

## Modifying IPtables
```
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
```

## Starting the hostspot
* `sudo /usr/sbin/hostapd /etc/hostapd/hostapd.conf`