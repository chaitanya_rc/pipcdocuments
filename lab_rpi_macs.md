## 1728 | B3
* eth0: `b8:27:eb:2a:bc:22`
* wlan0: `b8:27:eb:7f:e9:77`    -   Unblocked

## 1728 | B6 
* eth0: `b8:27:eb:71:1a:2d`
* wlan0: `b8:27:eb:24:4f:78`

## 1701 | C2
* eth0: `b8:27:eb:20:47:a0`     -   Unblocked
* wlan0: `b8:27:eb:75:12:f5`

## Black Red box 
* eth0: `b8:27:eb:a0:ef:9d`     -   Unblocked
* wlan0: `b8:27:eb:f5:ba:c8`    -   Unblocked

## 1719 | G5
* eth0: `b8:27:eb:b8:cc:3c`     -   Unblocked
* wlan0: `b8:27:eb:ed:99:69`